#!/usr/bin/perl
#
# Queries UDD for packages with given lintian tag(s) and maintainers
# Dependencies: JSON-XS, URI-Encode, autovivification, indirect, libwww-perl
#
# Copyright (C) 2023  P. J. McDermott
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

use English qw{-no_match_vars};
use strict;
use warnings;

use JSON::XS qw{decode_json};
use LWP::Simple;
use URI::Encode qw{uri_encode};
no autovivification;
no indirect;

sub udd_get_tag
{
	my ($tag, $email1, $email2, $email3, $sources) = @_;
	my $issues;
	my $source;
	my $information;

	$issues = get('https://udd.debian.org/lintian/?email1=' . $email1 .
		'&email2=' . $email2 . '&email3=' . $email3 .
		'&packages=&ignpackages=&format=json&lt_error=on' .
		'&lt_warning=on&lt_information=on&lt_pedantic=on' .
		'&lt_experimental=on&lt_overridden=on&lt_masked=on' .
		'&lt_classification=on&lintian_tag=' . $tag)
		or die('Failed to download issues from UDD');
	$issues = decode_json($issues)
		or die('Failed to decode JSON');

	foreach my $issue (@{$issues->{'all'}}) {
		$source = $issue->{'source'} . '/' . $issue->{'version'};
		$sources->{$source} = [] if not defined($sources->{$source});
		$information = $issue->{'information'};
		$information =~ s{(\\n|\n)$}{};
		if ($issue->{'tag_type'} eq 'overridden') {
			$information .= ' (overridden)';
		}
		push(@{$sources->{$source}}, $information);
	}

	return;
}

sub udd_get
{
	my ($tags, $emails) = @_;
	my $email1;
	my $email2;
	my $email3;
	my %sources;

	($email1, $email2, $email3) = split(qr{,}, $emails);
	$email1 ||= '';
	$email2 ||= '';
	$email3 ||= '';
	$email1 = uri_encode($email1, {'encode_reserved' => 1});
	$email2 = uri_encode($email2, {'encode_reserved' => 1});
	$email3 = uri_encode($email3, {'encode_reserved' => 1});
	foreach my $tag (split(qr{,}, $tags)) {
		udd_get_tag($tag, $email1, $email2, $email3, \%sources)
	}

	foreach my $source (sort(keys(%sources))) {
		STDOUT->printf("%s\n", $source);
		foreach my $information (@{$sources{$source}}) {
			STDOUT->printf("    %s\n", $information);
		}
	}

	return 1;
}

sub usage
{
	my ($fh) = @_;

	$fh->printf("Usage: %s tag[,tag...] email[,email[,email]]\n", $0);
}

sub main
{
	my $tags;
	my $emails;

	if ($#ARGV != 1) {
		usage(*STDERR);
		return 1;
	}
	($tags, $emails) = @ARGV;
	$emails ||= '';

	udd_get($tags, $emails);

	return 0;
}

exit(main());
