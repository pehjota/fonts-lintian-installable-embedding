#!/usr/bin/perl
#
# Finds reverse build dependencies of packages found with lintian.pl
# Dependencies: IO-Compress-Lzma, autovivification, indirect, libwww-perl
#
# Copyright (C) 2024  P. J. McDermott
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

use English qw{-no_match_vars};
use strict;
use warnings;

use IO::Uncompress::UnXz qw{unxz $UnXzError};
use LWP::Simple;
no autovivification;
no indirect;

sub parse_issues
{
	my ($issues) = @_;
	my $fh;
	my @pkgs;

	open($fh, '<', $issues)
		or die('Failed to open issues file "' . $issues . '"');
	while (my $line = readline($fh)) {
		next if $line =~ m{^    };
		$line =~ s{/.*$}{}s;
		push(@pkgs, $line);
	}
	close($fh)
		or die('Failed to close issues file "' . $issues . '"');

	return \@pkgs;
}

sub get_bins_and_rdeps
{
	my ($bins, $rdeps) = @_;
	my $xz;
	my $idx;
	my $pkg;
	my $bin;
	my $bdeps;

	foreach my $area (qw{main contrib non-free}) {
		$xz = get('https://deb.debian.org/debian/dists/unstable/' .
			$area . '/source/Sources.xz')
			or die('Failed to download Sources index');
		unxz(\$xz => \$idx, 'Append' => 1)
			or die('Failed to decompress Sources index: ' .
				$UnXzError);
	}

	foreach my $para (split(qr{\n\n}, $idx)) {
		($pkg)   = $para =~ m{^Package: (.+)$}m;
		($bin)   = $para =~ m{^Binary: (.+)$}m;
		($bdeps) = $para =~ m{^Build-Depends: (.+)$}m;
		$bins->{$pkg} = [split(qr{\s*,\s*}, $bin)];
		next if not defined($bdeps);
		foreach my $bdep (split(qr{\s*,\s*}, $bdeps)) {
			$bdep =~ s{ .*$}{};
			if (!defined($rdeps->{$bdep})) {
				$rdeps->{$bdep} = [];
			}
			push(@{$rdeps->{$bdep}}, $pkg);
		}
	}

	return;
}

sub usage
{
	my ($fh) = @_;

	$fh->printf("Usage: %s issues\n", $0);
}

sub main
{
	my $issues;
	my $issue_pkgs;
	my %bins;
	my %rdeps;
	my %pkg_rd;

	if ($#ARGV != 0) {
		usage(*STDERR);
		return 1;
	}
	($issues) = @ARGV;

	$issue_pkgs = parse_issues($issues);
	get_bins_and_rdeps(\%bins, \%rdeps);
	foreach my $pkg (@{$issue_pkgs}) {
		%pkg_rd = ();
		foreach my $bin (@{$bins{$pkg}}) {
			next if not defined($rdeps{$bin});
			foreach my $rdep (@{$rdeps{$bin}}) {
				$pkg_rd{$rdep} = 1;
			}
		}
		if (scalar(%pkg_rd) > 0) {
			print($pkg . ': ' . join(', ', sort(keys(%pkg_rd))) .
				"\n");
		}
	}

	return 0;
}

exit(main());
